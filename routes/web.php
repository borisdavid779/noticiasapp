<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('inicio', function () {
    return view('inicio');
});

Route::resource('usuarios','usuarioController');

//index, show, create,store,create,update,destroy
//mostrar todo, mostrar individual, cre<ar

Route::get('/', function () {
    return view('login');
});
//para gestion de
//para gestion de login
Route::resource('login','loginController');

Route::get('crear_password/{clave}', function($clave){
	//dd('$clave');
	return bcrypt($clave);
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

