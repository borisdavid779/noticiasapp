<?php

namespace App\Http\Controllers;

use App\usuarioModel;
use Illuminate\Http\Request;
use View;

class usuarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        //echo "Los usuarios son ";
        $usuarios=usuarioModel::all(['id', 'username']);
        //dd($usuarios);
        return View('admin.usuarios.index', compact('usuarios'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\usuarioModel  $usuarioModel
     * @return \Illuminate\Http\Response
     */
    public function show(usuarioModel $usuarioModel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\usuarioModel  $usuarioModel
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $usuario=usuarioModel::find($id);
        //dd($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\usuarioModel  $usuarioModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, usuarioModel $usuarioModel)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\usuarioModel  $usuarioModel
     * @return \Illuminate\Http\Response
     */
    public function destroy(usuarioModel $usuarioModel)
    {
        //
    }
}
