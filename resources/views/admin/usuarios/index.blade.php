@extends('layouts.padre')
@section('contenido')
<h1 align="center">Gestion de usuario</h1>
<div class="row">}
	<div class="col-lg-12">
		
		<table class="table">
			<thead>
				<tr>
					<th>ID</th>
					<th>NOMBRE</th>
					<th>EDITAR</th>
				</tr>
			</thead>
				<tbody>
					@foreach ($usuarios as $item)
					{{-- expr --}}
					<tr>
						<td>{{ $item->id }}</td>
						<td>{{$item->username}}</td>
						{{-- <td><a class="btn btn-success" href="{{ route('usuarios.edit')}}"> --}}
						<td> <a class="btn btn-success" href="usuarios/{{ $item->id }}/edit ">EDITAR</a></td>EDITAR</a></td>
					</tr>
					@endforeach
				</tbody>
		</table>
	</div>
</div>
@stop
