@extends('layouts.padre')
@section('contenido')
{!! Form::model($usuario, array('method' => 'PATCH', 'route' => array('usuarios.update', $usuario->id))) !!}

 <div class="form-group">
    {!! Form::label('username', 'Nombre de usuario:') !!}        
     {!! Form::text('username',null,['class'=>'form-control','placeholder'=>'Ingrese su usuairo del domicilio']) !!}

 </div>

  <div class="form-group">
    {!! Form::label('email', 'Correo electronico:') !!}        
            {!! Form::text('email',null,['class'=>'form-control','placeholder'=>'Ingrese su dirección del correo']) !!}
 </div>


  {!!Form::submit('Actualizar',['class'=>'btn btn-primary'])!!}
                 

{!! Form::close() !!}

@stop
